"""Simple transformation of sk-learn decision trees to predicates."""
from dataclasses import dataclass
from typing import Any
import jsonpickle


@dataclass
class And:
    left: "Predicate"
    right: "Predicate"


@dataclass
class Or:
    left: "Predicate"
    right: "Predicate"


@dataclass
class LessThan:
    left: "Value"
    right: "Value"


@dataclass
class GreaterThan:
    left: "Value"
    right: "Value"


@dataclass
class Feature:
    index: int


Value = (
    Feature |
    float
)


Predicate = (
    And |
    Or |
    LessThan |
    GreaterThan |
    bool
)


def predicate_to_str(predicate: Predicate) -> str:
    """Serialize predicate as a string."""
    return jsonpickle.encode(
        predicate,
        make_refs=False,
        indent=2,
    )


def type_check_value(obj: Any) -> bool:
    allowed_classes = (
        Feature,
        float,
    )
    if not isinstance(obj, allowed_classes):
        return False

    if isinstance(obj, float):
        return True

    # obj is Feature
    if not isinstance(obj.index, int):
        return False

    return True


def type_check_predicate(obj: Any) -> bool:
    allowed_classes = (
        And,
        Or,
        LessThan,
        GreaterThan,
        bool,
    )
    if not isinstance(obj, allowed_classes):
        return False

    if isinstance(obj, bool):
        return True

    if isinstance(obj, (And, Or)):
        if not type_check_predicate(obj.left):
            return False
        if not type_check_predicate(obj.right):
            return False
        return True

    # Obj is LessThan or GreaterThan
    if not type_check_value(obj.left):
        return False
    if not type_check_value(obj.right):
        return False
    return True


def str_to_predicate(predicate: str) -> Predicate:
    """Deserialize predicate from a string."""
    obj = jsonpickle.decode(
        predicate,
        on_missing="error",
    )
    assert type_check_predicate(obj)
    return obj


def get_value(
    value: Value,
    x: list[float],
) -> float:
    if isinstance(value, float):
        return value
    elif isinstance(value, Feature):
        return x[value.index]
    else:
        raise ValueError(
            f"Input of type '{type(value)}' not a predicate value!"
        )


def get_robustness_value(
    predicate: Predicate,
    x: list[float],
) -> float:
    """Get the robustness value of the predicate evaluated on the given
    input.

    A predicate is said to be True for a given input if and only if
    the corresponding robustness value is greater than zero.
    """
    if isinstance(predicate, bool):
        if predicate:
            return 1.0
        else:
            return -1.0
    elif isinstance(predicate, And):
        lv = get_robustness_value(predicate.left, x)
        rv = get_robustness_value(predicate.right, x)
        return min(lv, rv)
    elif isinstance(predicate, Or):
        lv = get_robustness_value(predicate.left, x)
        rv = get_robustness_value(predicate.right, x)
        return max(lv, rv)
    elif isinstance(predicate, LessThan):
        lv = get_value(predicate.left, x)
        rv = get_value(predicate.right, x)
        return rv - lv
    elif isinstance(predicate, GreaterThan):
        lv = get_value(predicate.left, x)
        rv = get_value(predicate.right, x)
        return lv - rv
    else:
        raise ValueError(f"Predicate of invalid type '{type(predicate)}'!")


def get_pretty_str(predicate: Predicate) -> str:
    def add_indent(s: str) -> str:
        lines = s.splitlines()
        lines = [f"\t{li}" for li in lines]
        return "\n".join(lines)

    def pretty_value(s: Value) -> str:
        if isinstance(s, (float, int)):
            return str(s)
        else:
            return f"x[{s.index}]"

    def recurse(predicate: Predicate) -> str:
        if isinstance(predicate, bool):
            if predicate:
                return "True"
            else:
                return "False"
        elif isinstance(predicate, And):
            ls = recurse(predicate.left)
            rs = recurse(predicate.right)
            ls = add_indent(ls)
            rs = add_indent(rs)
            ls = f"(\n{ls}\n)"
            rs = f"(\n{rs}\n)"
            return f"{ls}\nAND\n{rs}"
        elif isinstance(predicate, Or):
            ls = recurse(predicate.left)
            rs = recurse(predicate.right)
            ls = add_indent(ls)
            rs = add_indent(rs)
            ls = f"(\n{ls}\n)"
            rs = f"(\n{rs}\n)"
            return f"{ls}\nOR\n{rs}"
        elif isinstance(predicate, LessThan):
            ls = pretty_value(predicate.left)
            rs = pretty_value(predicate.right)
            return f"{ls} < {rs}"
        elif isinstance(predicate, GreaterThan):
            ls = pretty_value(predicate.left)
            rs = pretty_value(predicate.right)
            return f"{ls} > {rs}"
        else:
            raise ValueError(f"Predicate of invalid type '{type(predicate)}'!")

    return recurse(predicate)
