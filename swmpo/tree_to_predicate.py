"""Simple transformation of sk-learn decision trees to predicates."""
from sklearn.tree import DecisionTreeClassifier
from functools import reduce
from swmpo.predicates import Predicate
from swmpo.predicates import Feature
from swmpo.predicates import LessThan
from swmpo.predicates import GreaterThan
from swmpo.predicates import And
from swmpo.predicates import Or


def tree_to_predicate(tree: DecisionTreeClassifier) -> Predicate:
    """Transform the decision tree to a predicate.

    It is assumed that there are only two classes: "1" and "0", which
    correspond to True and False, respectively.
    """
    # Check special case where there is only one class
    classes = list(tree.classes_)
    if classes == [0]:
        return False
    elif classes == [1]:
        return True

    tree_ = tree.tree_

    def find_true_branches(
        node: int,
        branch_predicate: Predicate | None,
    ) -> list[Predicate]:
        is_leaf = tree_.children_left[node] == tree_.children_right[node]

        if not is_leaf:
            feature_i = int(tree_.feature[node])
            feature = Feature(feature_i)
            threshold = float(tree_.threshold[node])
            left_branch_predicate = LessThan(
                left=feature,
                right=threshold,
            )
            right_branch_predicate = GreaterThan(
                left=feature,
                right=threshold,
            )

            if branch_predicate is not None:
                left_branch_predicate = And(
                    branch_predicate,
                    left_branch_predicate,
                )
                right_branch_predicate = And(
                    branch_predicate,
                    right_branch_predicate,
                )

            true_left_branches = find_true_branches(
                tree_.children_left[node],
                left_branch_predicate,
            )
            true_right_branches = find_true_branches(
                tree_.children_right[node],
                right_branch_predicate,
            )

            true_branches = true_left_branches + true_right_branches
            return true_branches

        else:
            # Node is a leaf
            class_weights = tree_.value[node]
            class0_weight, class1_weight = class_weights[0]

            if class0_weight > class1_weight:
                # Leaf is False. There are no True branches
                return list()
            elif branch_predicate is not None:
                # Leaf is True.
                return [branch_predicate]
            else:
                return list()

    true_predicates = find_true_branches(0, None)
    if len(true_predicates) == 0:
        return False
    if len(true_predicates) == 1:
        return true_predicates[0]
    return reduce(Or, true_predicates)
