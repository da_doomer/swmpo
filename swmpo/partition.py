"""Partition of a dataset of state transitions guided by synthesis of
local neural models."""
from dataclasses import dataclass
from swmpo.transition import Transition
from swmpo.transition import get_vector
from swmpo.transition import serialize
from swmpo.transition import deserialize
from swmpo.world_models.model import get_input_output_size
from swmpo.world_models.world_model import serialize_model
from swmpo.world_models.world_model import deserialize_model
from swmpo.world_models.world_model import WorldModel
from swmpo.world_models.world_model import get_optimized_model
from swmpo.world_models.mode_world_model import ModeWorldModel
from swmpo.world_models.mode_world_model import get_predictive_residual_encoder
from swmpo.world_models.mode_world_model import get_mode_vector
from swmpo.transition_prunning.island_prunning import prune_short_transitions
from sklearn.preprocessing import StandardScaler
from functools import cached_property
import sklearn.cluster
import random
import torch
import tempfile
from pathlib import Path
import shutil
import umap
import json
import zipfile

# Avoid pytorch from doing threading. This is so that the script doesn't
# take over the computer's resources. You can remove these lines if not running
# on a lab computer.
torch.set_num_threads(1)


@dataclass
class StatePartitionItem:
    """A partition of a dataset of partitions."""
    local_model: WorldModel
    subset: list[Transition]
    hidden_sizes: list[int]

    @property
    def local_model_input_size(self) -> int:
        input_size, _ = get_input_output_size(self.subset[0])
        return input_size

    @property
    def local_model_output_size(self) -> int:
        _, output_size = get_input_output_size(self.subset[0])
        return output_size

    @cached_property
    def transition_vectors_as_set_of_tuples(self) -> set[tuple[float, ...]]:
        vectors = set[tuple[float, ...]]()
        for transition in self.subset:
            x = get_vector(transition)
            vectors.add(tuple[float, ...](x.tolist()))
        return vectors


def serialize_partition_item(
    partition_item: StatePartitionItem,
    output_zip_path: Path,
):
    """Serialize a partition item to a ZIP file."""
    assert output_zip_path.suffix == ".zip"

    with tempfile.TemporaryDirectory() as tmpdirname:
        output_dir = Path(tmpdirname)

        # Serialize local model
        model_path = output_dir/"model.zip"
        serialize_model(
            model=partition_item.local_model,
            input_size=partition_item.local_model_input_size,
            output_size=partition_item.local_model_output_size,
            hidden_sizes=partition_item.hidden_sizes,
            output_zip_path=model_path,
        )

        # Serialize subset of transitions
        subset_dir = output_dir/"transition_subset"
        subset_dir.mkdir()
        transition_directory = list[str]()
        for i, transition in enumerate(partition_item.subset):
            transition_zip = f"transition_{i}.zip"
            serialize(transition, subset_dir/transition_zip)
            transition_directory.append(str(transition_zip))

        # Serialize directory
        transition_directory_json_path = output_dir/"transition_directory.json"
        with open(transition_directory_json_path, "wt") as fp:
            json.dump(transition_directory, fp, indent=2)

        # Serialize hidden sizes
        hidden_sizes_json_path = output_dir/"hidden_sizes.json"
        with open(hidden_sizes_json_path, "wt") as fp:
            json.dump(partition_item.hidden_sizes, fp, indent=2)

        # ZIP directory
        _ = shutil.make_archive(
            str(output_zip_path.with_suffix("")),
            'zip',
            output_dir
        )


def deserialize_partition_item(
    zip_path: Path,
) -> StatePartitionItem:
    """Deserialize a partition item from a ZIP file."""
    with tempfile.TemporaryDirectory() as tmpdirname:
        output_dir = Path(tmpdirname)

        with zipfile.ZipFile(zip_path, "r") as zip_ref:
            zip_ref.extractall(output_dir)

        # Load model
        model_path = output_dir/"model.zip"
        local_model = deserialize_model(model_path)

        # Load transition directory
        transition_directory_json_path = output_dir/"transition_directory.json"
        with open(transition_directory_json_path, "rt") as fp:
            transition_directory = list[str](json.load(fp))

        # Load transitions
        subset = list[Transition]()
        for transition_path in transition_directory:
            zip_path = output_dir/"transition_subset"/transition_path
            transition = deserialize(zip_path)
            subset.append(transition)

        # Load hidden sizes
        hidden_sizes_json_path = output_dir/"hidden_sizes.json"
        with open(hidden_sizes_json_path, "rt") as fp:
            hidden_sizes = list[int](json.load(fp))

    item = StatePartitionItem(
        local_model=local_model,
        subset=subset,
        hidden_sizes=hidden_sizes,
    )
    return item


def serialize_partition(
    partition: list[StatePartitionItem],
    output_zip_path: Path,
):
    assert output_zip_path.suffix == ".zip"

    with tempfile.TemporaryDirectory() as tmpdirname:
        output_dir = Path(tmpdirname)

        # Serialize each item
        item_directory = list[str]()
        for i, item in enumerate(partition):
            item_path = f"item_{i}.zip"
            serialize_partition_item(
                partition_item=item,
                output_zip_path=output_dir/item_path,
            )
            item_directory.append(item_path)

        # Serialize directory
        item_directory_json_path = output_dir/"item_directory.json"
        with open(item_directory_json_path, "wt") as fp:
            json.dump(item_directory, fp, indent=2)

        # ZIP directory
        _ = shutil.make_archive(
            str(output_zip_path.with_suffix("")),
            'zip',
            output_dir
        )


def deserialize_partition(zip_path: Path) -> list[StatePartitionItem]:
    items = list[StatePartitionItem]()

    with tempfile.TemporaryDirectory() as tmpdirname:
        output_dir = Path(tmpdirname)

        with zipfile.ZipFile(zip_path, "r") as zip_ref:
            zip_ref.extractall(output_dir)

        # Load directory
        item_directory_json_path = output_dir/"item_directory.json"
        with open(item_directory_json_path, "rt") as fp:
            item_paths = list[str](json.load(fp))

        # Load each partition item
        for item_path in item_paths:
            zip_path = output_dir/item_path
            item = deserialize_partition_item(zip_path)
            items.append(item)

    return items


class PartitionSortingError(Exception):
    pass


def item_contains_transition(
        item: StatePartitionItem,
        transition: Transition,
        ) -> bool:
    """Return whether the transition appears
    in the partition item."""
    x = tuple(get_vector(transition).tolist())
    return x in item.transition_vectors_as_set_of_tuples


def get_initial_transition_n(
        item: StatePartitionItem,
        episodes: list[list[Transition]],
        ) -> int:
    """Return the number of times a transition occurs in the partition item."""
    # Extract initial transitions
    initial_transitions = [
        episode[0]
        for episode in episodes
        if len(episode) > 0
    ]

    # Filter-in the initial transitions that
    # appear in the partition item
    occurrences = [
        initial_transition
        for initial_transition in initial_transitions
        if item_contains_transition(
            item=item,
            transition=initial_transition
        )
    ]
    return len(occurrences)


def get_sorted_partition(
        partition: list[StatePartitionItem],
        episodes: list[list[Transition]],
        ) -> list[StatePartitionItem]:
    """Sort the partition so that the partition item with the most
    initial transitions is first."""
    # Identify first item
    sorted_partition = list(reversed(sorted(
        partition,
        key=lambda item: get_initial_transition_n(
            item=item,
            episodes=episodes
        )
    )))
    return sorted_partition


def get_partition_modes(
    trajectory: list[Transition],
    partition: list[StatePartitionItem],
) -> list[int]:
    """Return the list of indices of each transition in the trajectory."""
    modes = list[int]()
    for transition in trajectory:
        index = None
        for i, item in enumerate(partition):
            if item_contains_transition(item, transition):
                index = i
        assert index is not None, "Partition doesn't contain transition!"
        modes.append(index)
    return modes


def get_clusters(
    mode_world_model: ModeWorldModel,
    trajectories: list[list[Transition]],
    cluster_n: int,
    min_island_size: int,
    dimensionality_reduce: int | None,
    seed: str,
    device: str,
) -> list[set[tuple[int, int]]]:
    """Partition the given dataset of transitions into disjoint subsets.
    The returned sets contain the indices of the transitions in the set."""
    _random = random.Random(seed)

    # Bookkeeping
    vector_indices = list[tuple[int, int]]()
    location_index = dict[tuple[int, int], int]()
    for i, trajectory in enumerate(trajectories):
        for j, _ in enumerate(trajectory):
            location = (i, j)
            index = len(vector_indices)
            vector_indices.append(location)
            location_index[location] = index

    # Get the latent vector for each transition
    encoded_vectors = list[list[float]]()
    for trajectory in trajectories:
        for transition in trajectory:
            embedding = get_mode_vector(
                transition,
                mode_world_model=mode_world_model,
                device=device,
            )
            encoded_vectors.append(embedding)
    X = torch.tensor(encoded_vectors)

    # Normalize embeddings
    X = StandardScaler().fit_transform(X)
    if dimensionality_reduce is not None:
        reducer = umap.UMAP(
            n_components=dimensionality_reduce,
            random_state=int.from_bytes(_random.randbytes(3), 'big', signed=False),
        )
        X = reducer.fit_transform(X)

    # Cluster latent vectors
    cluster = sklearn.cluster.KMeans(
        n_clusters=cluster_n,
        random_state=int.from_bytes(_random.randbytes(3), 'big', signed=False),
    )
    labels = list[int](cluster.fit_predict(X))

    for trajectory in trajectories:
        assert len(trajectory) > 0

    # Prune short transitions
    new_labels = list(labels)
    for i, trajectory in enumerate(trajectories):
        # Reconstruct sequence of assigned modes
        modes = list[int]()
        for j, transition in enumerate(trajectory):
            location = (i, j)
            index = location_index[location]
            mode = labels[index]
            modes.append(mode)

        # Prune sequence of modes
        new_modes = prune_short_transitions(modes, min_island_size)

        # Add new labels
        for j, new_mode in enumerate(new_modes):
            location = (i, j)
            index = location_index[location]
            new_labels[index] = new_mode
    labels = new_labels

    # Assemble clusters
    clusters = [
        set[tuple[int, int]]()
        for _ in range(cluster_n)
    ]
    for i, cluster_i in enumerate(labels):
        location = vector_indices[i]
        clusters[cluster_i].add(location)

    # Remove empty clusters
    clusters = [
        cluster
        for cluster in clusters
        if len(cluster) > 0
    ]
    return clusters


@dataclass
class OptimizationResult:
    partition: list[StatePartitionItem]
    loss_log: list[float]
    mode_world_model: ModeWorldModel


def get_optimized_error_partition(
    trajectories: list[list[Transition]],
    hidden_sizes: list[int],
    learning_rate: float,
    latent_size: int,
    iter_n: int,
    clustering_dimensionality_reduce: int | None,
    clustering_information_content_regularization_scale: float,
    clustering_mutual_information_regularization_scale: float,
    local_model_hyperparameters: dict[str, str | int | float],
    dt: float,
    size: int,
    seed: str,
    min_island_size: int,
    batch_size: int,
    mutual_information_mini_batch_size: int,
    device: str,
    verbose: bool,
) -> OptimizationResult:
    """Helper function to optimize the partition ensemble by error weighting."""
    _random = random.Random(seed)

    # Normalize trajectory vectors
    if verbose:
        print("Normalizing trajectories for mode_world_model")

    # Train mode_world_model
    mode_world_model = get_predictive_residual_encoder(
        trajectories=trajectories,
        hidden_sizes=hidden_sizes,
        learning_rate=learning_rate,
        latent_size=latent_size,
        iter_n=iter_n,
        seed=str(_random.random()),
        dt=dt,
        batch_size=batch_size,
        information_content_regularization_scale=clustering_information_content_regularization_scale,
        mutual_information_regularization_scale=clustering_mutual_information_regularization_scale,
        mutual_information_mini_batch_size=mutual_information_mini_batch_size,
        device=device,
        verbose=verbose,
    )

    # Cluster latent state
    clusters = get_clusters(
        mode_world_model=mode_world_model,
        trajectories=trajectories,
        cluster_n=size,
        min_island_size=min_island_size,
        dimensionality_reduce=clustering_dimensionality_reduce,
        seed=str(_random.random()),
        device=device,
    )

    # Assemble partition items
    partition = list[StatePartitionItem]()
    for cluster in clusters:

        cluster_sub_trajectories = list[list[Transition]]()

        for i, trajectory in enumerate(trajectories):
            sub_trajectory = list[Transition]()

            for j, t in enumerate(trajectory):
                if (i, j) not in cluster:
                    if len(sub_trajectory) > 0:
                        # We have transitioned out of the cluster
                        cluster_sub_trajectories.append(sub_trajectory)
                        sub_trajectory = list[Transition]()
                else:
                    # We continue building the sub-trajectory
                    sub_trajectory.append(t)

            if len(sub_trajectory) > 1:
                cluster_sub_trajectories.append(sub_trajectory)

        subset = [
            trajectories[i][j]
            for (i, j) in cluster
        ]
        model = get_optimized_model(
            hyperparameters=local_model_hyperparameters,
            trajectories=cluster_sub_trajectories,
            dt=dt,
            seed=str(_random.random()),
        )
        partition_item = StatePartitionItem(
            local_model=model,
            subset=subset,
            hidden_sizes=hidden_sizes,
        )
        partition.append(partition_item)

    # Assemble result
    loss_log = mode_world_model.loss_log
    optimization_result = OptimizationResult(
        partition=partition,
        loss_log=loss_log,
        mode_world_model=mode_world_model,
    )
    return optimization_result


def get_partition(
    episodes: list[list[Transition]],
    hidden_sizes: list[int],
    learning_rate: float,
    latent_size: int,
    mode_model_iter_n: int,
    clustering_dimensionality_reduce: int | None,
    clustering_information_content_regularization_scale: float,
    clustering_mutual_information_regularization_scale: float,
    dt: float,
    size: int,
    min_island_size: int,
    seed: str,
    batch_size: int,
    local_model_hyperparameters: dict[str, str | int | float],
    mutual_information_mini_batch_size: int,
    device: str,
    verbose: bool,
) -> OptimizationResult:
    """Returns a partition of the set of transitions in the given episodes.
    Each subset of the partition has a corresponding neural model of the
    dynamics in that subset.

    The returned state machine will be sorted so that the first item contains
    the first transition in the input episodes. If that is not possible,
    `PartitionSortingError` will be raised.
    """
    _random = random.Random(seed)

    # Optimize smooth partition
    optimization_result = get_optimized_error_partition(
        trajectories=episodes,
        hidden_sizes=hidden_sizes,
        learning_rate=learning_rate,
        iter_n=mode_model_iter_n,
        dt=dt,
        latent_size=latent_size,
        clustering_dimensionality_reduce=clustering_dimensionality_reduce,
        clustering_information_content_regularization_scale=clustering_information_content_regularization_scale,
        clustering_mutual_information_regularization_scale=clustering_mutual_information_regularization_scale,
        local_model_hyperparameters=local_model_hyperparameters,
        size=size,
        verbose=verbose,
        batch_size=batch_size,
        mutual_information_mini_batch_size=mutual_information_mini_batch_size,
        min_island_size=min_island_size,
        device=device,
        seed=str(_random.random()),
    )

    # Sort state partition so that the first item contains the first
    # transition of the episodes
    state_partition = get_sorted_partition(
        episodes=episodes,
        partition=optimization_result.partition,
    )

    optimization_result = OptimizationResult(
        partition=state_partition,
        loss_log=optimization_result.loss_log,
        mode_world_model=optimization_result.mode_world_model,
    )
    return optimization_result
