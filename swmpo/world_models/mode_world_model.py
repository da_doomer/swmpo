from dataclasses import dataclass
from swmpo.transition import Transition
from swmpo.transition_normalization import VectorStats
from swmpo.transition_normalization import get_vector_stats
from swmpo.transition_normalization import get_normalized_vector
from swmpo.world_models.model import get_relu_mlp
from itertools import product
import random
import torch
from torch.distributions.multivariate_normal import MultivariateNormal


@dataclass
class ModeWorldModel:
    encoder: torch.nn.Module
    decoder: torch.nn.Module
    state_normalization: VectorStats
    action_normalization: VectorStats
    loss_log: list[float]


def get_mode_vector(
    transition: Transition,
    mode_world_model: ModeWorldModel,
    device: str,
) -> list[float]:
    """Get the embedded representation of the given transition."""
    st = get_normalized_vector(
        transition.source_state,
        mode_world_model.state_normalization,
    )
    at = get_normalized_vector(
        transition.action,
        mode_world_model.action_normalization,
    )
    stp1 = get_normalized_vector(
        transition.next_state,
        mode_world_model.state_normalization,
    )
    sast = torch.cat([st, at, stp1]).to(device)
    encoded_vector = mode_world_model.encoder(sast.unsqueeze(0)).squeeze()
    return list[float](encoded_vector.detach().cpu().numpy().tolist())


def get_mean_information_content(
    X: torch.Tensor,  # shape (vector_n, feature_n)
    device: str,
) -> torch.Tensor:
    """Return the mean information content of the given set of vectors
    with respect to a multi-variate Gaussian distribution centered at 0
    with unit variance.

    We assume features are independent.
    """
    # Get the log probability of each vector
    _, feature_n = X.size()
    loc = torch.zeros((feature_n,), device=device)
    scale = torch.ones((feature_n,), device=device)
    # Because we assume the variables are independent, then we use
    # a diagonal covariance matrix
    covariance_matrix = torch.diag(scale)
    n = MultivariateNormal(
        loc=loc,
        covariance_matrix=covariance_matrix,
    )
    log_probs = n.log_prob(X)
    return (-log_probs).sum().mean()


def get_distribution(X: torch.Tensor) -> MultivariateNormal:
    """Return the Gaussian that maximizes the likelihood of the given
    list of vectors. X is assumed to be of shape (sample_n, feature_n).

    (This assumes variables
    """
    sample_n, feature_n = X.size()
    features = X.T
    feature_stdevs, feature_means = torch.std_mean(features, dim=1)

    # Because we assume the variables are independent, then we use
    # a diagonal covariance matrix
    covariance_matrix = torch.diag(feature_stdevs)

    assert tuple(feature_means.size()) == (feature_n,)
    assert tuple(covariance_matrix.size()) == (feature_n, feature_n)
    n = MultivariateNormal(
        loc=feature_means,
        covariance_matrix=covariance_matrix,
    )
    return n


def get_mutual_information(
    X: torch.Tensor,
    Y: torch.Tensor,
    mini_batch_size: int,
    seed: str,
) -> torch.Tensor:
    """Approximate the mutual information between X and Y.

    This assumes that the distributions P_X, P_Y and P_(X, Y) are
    Gaussians.

    X and Y are assumed to be of shape (sample_n, features1_n) and
    (sample_n, features2_n) respectively.
    """
    # Characterize P(X, Y), P(X) and P(Y)
    PX = get_distribution(X)
    PY = get_distribution(Y)
    XY = torch.cat([X, Y], dim=1)
    PXY = get_distribution(XY)

    # Formula is
    # I(X, Y) = \int x in X \int y in Y P(x, y) * log(P(x, y)/(P(x)*P(y))) dxdy
    #         = \int x in X \int y in Y P(x, y) * (log(P(x, y)) - log(P(x)*log(P(y)))) dxdy
    #         = \int x in X \int y in Y P(x, y) * (log(P(x, y)) - (log(P(x)) + log(P(y)))) dxdy
    #
    # Making \int and log(P(-)) notation more concise
    #
    #         = int x int y  P(x, y) * (logP(x, y) - (logP(x) + logP(y))) dxdy
    #         = int x int y  P(x, y) * (logP(x, y) - logP(x) - logP(y)) dxdy

    log_pX = PX.log_prob(X)
    log_pY = PY.log_prob(Y)

    # Speed:
    # This was really slow, so it ended up looking a bit confusing when
    # I tried to maximize the number of operations done with pytorch
    # to leverage SIMD
    # But it still wasn't fast enough, so I will sample a subset of the
    # pairs over which the integral is performed.
    _random = random.Random(seed)
    ijs = list(product(range(len(X)), range(len(Y))))
    minibatch_ijs = _random.sample(
        ijs,
        k=min(mini_batch_size, len(ijs))
    )

    pairs = list()
    Slog_pX_list = list()
    Slog_pY_list = list()
    for i, j in minibatch_ijs:
        Slog_pX_list.append(log_pX[i])
        Slog_pY_list.append(log_pY[j])
        pairs.append(torch.cat([X[i], Y[j]]))
    Slog_pX = torch.stack(Slog_pX_list)
    Slog_pY = torch.stack(Slog_pY_list)
    log_pXY = PXY.log_prob(torch.stack(pairs))
    pXY = log_pXY.exp()

    assert Slog_pX.size() == Slog_pY.size()
    assert Slog_pY.size() == log_pXY.size()
    assert log_pXY.size() == pXY.size()

    IXY = pXY * (log_pXY - Slog_pX - Slog_pY)
    # /Speed

    return torch.mean(IXY)


def get_predictive_residual_encoder(
    trajectories: list[list[Transition]],
    hidden_sizes: list[int],
    latent_size: int,
    learning_rate: float,
    iter_n: int,
    dt: float,
    seed: str,
    batch_size: int,
    information_content_regularization_scale: float,
    mutual_information_regularization_scale: float,
    mutual_information_mini_batch_size: int,
    device: str,
    verbose: bool,
) -> ModeWorldModel:
    """Return an mode_world_model optimized on the given dataset of
    transitions."""
    _random = random.Random(seed)

    # Find size of the states
    state_size = len(trajectories[0][0].source_state)
    action_size = len(trajectories[0][0].action)

    # Initialize local models
    encoder = get_relu_mlp(
        input_size=state_size+action_size+state_size,
        hidden_sizes=hidden_sizes,
        output_size=latent_size,
        seed=str(_random.random()),
    ).to(device).train(True)
    decoder = get_relu_mlp(
        input_size=latent_size+state_size+action_size,
        hidden_sizes=hidden_sizes,
        output_size=state_size,
        seed=str(_random.random()),
    ).to(device).train(True)

    # Initialize optimization algorithm
    parameters = list()
    parameters.extend(encoder.parameters())
    parameters.extend(decoder.parameters())
    optimizer = torch.optim.Adam(
        params=parameters,
        lr=learning_rate,
    )
    optimizer.zero_grad()

    # Build regression targets
    # tm1 is "t minus 1". E.g., stm1 = s{t-1}
    # Similarly, tp1 is "t plus 1".
    St_list = list()
    At_list = list()
    Stm1_list = list()
    Atm1_list = list()
    Stp1_list = list()
    for trajectory in trajectories:
        for t, transition in enumerate(trajectory[1:]):
            st = trajectory[t].source_state
            at = trajectory[t].action
            stm1 = trajectory[t-1].source_state
            atm1 = trajectory[t-1].action
            stp1 = trajectory[t].next_state

            St_list.append(st.tolist())
            At_list.append(at.tolist())
            Stm1_list.append(stm1.tolist())
            Atm1_list.append(atm1.tolist())
            Stp1_list.append(stp1.tolist())

    state_normalization = get_vector_stats(St_list)
    action_normalization = get_vector_stats(At_list)

    St_list = [
        get_normalized_vector(torch.tensor(x), state_normalization).tolist()
        for x in St_list
    ]
    At_list = [
        get_normalized_vector(torch.tensor(x), action_normalization).tolist()
        for x in At_list
    ]
    Stm1_list = [
        get_normalized_vector(torch.tensor(x), state_normalization).tolist()
        for x in Stm1_list
    ]
    Atm1_list = [
        get_normalized_vector(torch.tensor(x), action_normalization).tolist()
        for x in Atm1_list
    ]
    Stp1_list = [
        get_normalized_vector(torch.tensor(x), state_normalization).tolist()
        for x in Stp1_list
    ]

    St = torch.tensor(St_list)
    At = torch.tensor(At_list)
    Stm1 = torch.tensor(Stm1_list)
    Atm1 = torch.tensor(Atm1_list)
    Stp1 = torch.tensor(Stp1_list)

    # DEBUG
    St = St.to(device=device)
    At = At.to(device=device)
    Stm1 = Stm1.to(device=device)
    Atm1 = Atm1.to(device=device)
    Stp1 = Stp1.to(device=device)
    # /DEBUG

    mse = torch.nn.MSELoss()

    # Run gradient descent
    loss_log = list()
    indices = list(range(len(St)))
    print(f"[get_predictive_residual_encoder] Dataset size: {len(indices)}")

    # To avoid clutering stdout, print 10 times during the training
    print_iter_is = [0, iter_n-1]
    print_iter_is.extend(list(range(0, iter_n, max(iter_n//10, 1))))

    for iter_i in range(iter_n):

        # Sample batch
        batch_is = _random.sample(indices, k=min(batch_size, len(indices)))

        #St_batch = St[batch_is].to(device=device)
        #At_batch = At[batch_is].to(device=device)
        #Stm1_batch = Stm1[batch_is].to(device=device)
        #Atm1_batch = Atm1[batch_is].to(device=device)
        #Stp1_batch = Stp1[batch_is].to(device=device)
        St_batch = St[batch_is]
        At_batch = At[batch_is]
        Stm1_batch = Stm1[batch_is]
        Atm1_batch = Atm1[batch_is]
        Stp1_batch = Stp1[batch_is]

        # Get joint variable SAS_{t} = (S_{t-1}, A_{t-1}, S_{t})
        SASt_batch = torch.cat([Stm1_batch, Atm1_batch, St_batch], dim=1)

        # Get latent mode variable M_{t}
        Mt_batch = encoder(SASt_batch)

        # Get joint variable MA_{t} = (M_{t}, A_{t})
        MAt_batch = torch.cat([Mt_batch, St_batch, At_batch], dim=1)

        # Get model output
        DeltaSt_batch_predicted = decoder(MAt_batch)

        # Get model errors
        Stp1_batch_predicted = St_batch + DeltaSt_batch_predicted*dt

        # Get information bottleneck regularization term
        information_content = information_content_regularization_scale * \
            get_mean_information_content(MAt_batch, device)

        # Get mutual information bottleneck regularization term
        mutual_information = mutual_information_regularization_scale *\
            get_mutual_information(
                MAt_batch,
                Stp1_batch,
                seed=str(_random.random()),
                mini_batch_size=mutual_information_mini_batch_size,
            )

        # Aggregate weighted errors
        target = Stp1_batch
        output = Stp1_batch_predicted
        loss = mse(target, output)
        loss = loss + information_content
        loss = loss + mutual_information

        # Step optimization algorithm
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        # Log losses
        loss_log.append(loss.item())

        if verbose and iter_i in print_iter_is:
            print(f"[get_predictive_residual_encoder] Iter: {iter_i}/{iter_n}; Loss: {loss.item()}")

    mode_world_model = ModeWorldModel(
        encoder=encoder.eval(),
        decoder=decoder.eval(),
        state_normalization=state_normalization,
        action_normalization=action_normalization,
        loss_log=loss_log,
    )
    return mode_world_model
