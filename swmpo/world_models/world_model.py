from dataclasses import dataclass
from swmpo.transition import Transition
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPRegressor
from joblib import dump, load
import random
import torch
import tempfile
import shutil
from pathlib import Path
import zipfile


@dataclass
class WorldModel:
    """Simple nearest neighbor world model."""
    source_scaler: StandardScaler
    next_scaler: StandardScaler
    regressor: MLPRegressor

    def get_prediction(
        self,
        source_state: torch.Tensor,
        action: torch.Tensor,
        dt: float,
    ) -> torch.Tensor:
        """Helper function to output the prediction of the model."""
        source_v = torch.cat([source_state, action]).detach().cpu().tolist()
        source_v = self.source_scaler.transform([source_v])

        predicted = self.regressor.predict(source_v)

        predicted = self.next_scaler.inverse_transform(predicted)

        return torch.from_numpy(predicted[0])

    def get_raw_error(
        self,
        transition: Transition,
        dt: float,
    ) -> float:
        """Helper function to get the L2 distance between the prediction
        of the model and the ground-truth data."""
        predicted = self.get_prediction(
            source_state=transition.source_state,
            action=transition.action,
            dt=dt,)
        error = (predicted - transition.next_state).norm()
        return float(error.item())


def serialize_model(
    model: WorldModel,
    input_size: int,
    output_size: int,
    hidden_sizes: list[int],
    output_zip_path: Path,
):
    """Serialize the given model to a ZIP file."""
    assert output_zip_path.suffix == ".zip"
    with tempfile.TemporaryDirectory() as tmpdirname:
        output_dir = Path(tmpdirname)

        source_scaler_path = output_dir/"source_scaler.joblib"
        dump(model.source_scaler, source_scaler_path)

        next_scaler_path = output_dir/"next_scaler.joblib"
        dump(model.next_scaler, next_scaler_path)

        regressor_path = output_dir/"regressor.joblib"
        dump(model.regressor, regressor_path)

        # ZIP directory
        _ = shutil.make_archive(
            str(output_zip_path.with_suffix("")),
            'zip',
            output_dir
        )


def deserialize_model(
    zip_path: Path,
) -> WorldModel:
    with tempfile.TemporaryDirectory() as tmpdirname:
        output_dir = Path(tmpdirname)

        with zipfile.ZipFile(zip_path, "r") as zip_ref:
            zip_ref.extractall(output_dir)

        source_scaler_path = output_dir/"source_scaler.joblib"
        source_scaler = load(source_scaler_path)

        next_scaler_path = output_dir/"next_scaler.joblib"
        next_scaler = load(next_scaler_path)

        regressor_path = output_dir/"regressor.joblib"
        regressor = load(regressor_path)

        world_model = WorldModel(
            source_scaler=source_scaler,
            next_scaler=next_scaler,
            regressor=regressor,
        )

    return world_model


def get_optimized_model(
    trajectories: list[list[Transition]],
    hyperparameters: dict[str, str | int | float],
    dt: float,  # unused for now
    seed: str,
) -> WorldModel:
    """Helper function to optimize the partition ensemble by error
    weighting."""
    source_v = list[list[float]]()
    next_v = list[list[float]]()
    _random = random.Random(seed)

    print(f"[get_optimized_model] Trajectory number: {len(trajectories)}")

    for trajectory in trajectories:
        for transition in trajectory:
            v = torch.cat([transition.source_state, transition.action]).detach().cpu().tolist()
            nv = transition.next_state.detach().cpu().tolist()
            source_v.append(v)
            next_v.append(nv)

    source_scaler = StandardScaler()
    next_scaler = StandardScaler()

    source_scaler.fit(source_v)
    next_scaler.fit(next_v)

    source_v = source_scaler.transform(source_v)
    next_v = next_scaler.transform(next_v)

    regressor = MLPRegressor(
        random_state=_random.getrandbits(32),
        **hyperparameters,
    )
    regressor.fit(source_v, next_v)

    return WorldModel(
        source_scaler=source_scaler,
        next_scaler=next_scaler,
        regressor=regressor,
    )
