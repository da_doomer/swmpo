# SWMPO

[Documentation](https://da_doomer.gitlab.io/swmpo)

*Structured World Modelling for Policy Optimization** (SWMPO) is a framework for
solving Markov Decision Processes.

## TLDR

SWMPO consists of two parts: world modelling as a hybrid system, and leveraging
the structured world model for policy optimization.

## Assumptions

### Assumption 1

The framework assumes existence of an agent that interacts with its
environment, deciding actions sequentially based on observations of the
environment.

### Assumption 2

The goal is to find a policy (mapping from observations to actions) that
approximately maximizes rewards.

### Assumption 3

Crucially, the robot and the environment are assumed to behave as a hybrid system.
More precisely, the framework assumes that, when viewed as a dynamical system,
the evolution of the world depends on both continuous variables and a discrete mode.

For example, when viewed as a dynamical system, an amphibious robot will behave
differently when walking (the "land" mode) and swimming (the "water" mode).

### Assumption 4

The framework assumes that the hybrid system can be described as a finite state
machine, where each node in the state machine corresponds to a mode.

### Assumption 5

An initial controller that can be used to collect data from the environment
must be available.

## API

There are two main steps:

- World modelling: transforms a dataset of observations into a state machine
- Policy optimization: augments an MDP with a state machine for policy optimization

### World Modelling

The world modelling step takes as input a set of *trajectories* (each
trajectory is a sequence of (observation, action, next observation) triples;
each of these triples is called a *transition*), and a number of states, and
outputs a state machine with that number of states that approximates the
dynamics of the dynamical system.

#### Partitioning

Partitioning a dataset is implemented in `swmpo.partition_by_errors.get_partition`.

The first step is to find out which transitions in the input dataset correspond
to each state of the state machine.

Fundamentally, each state in the state machine is a function of the
form `(observation, action) -> next observation`. That is, each state
is an approximation of the transition function of the environment for a subset
of the state space.

#### Turning partitions into state machines

Turning a partition into a state machine is implemented in `swmpo.state_machine.get_partition_induced_state_machine`.

Once a dataset has been partitioned, the transition dynamics between modes can
be inferred.

Each transition answers the question: "if the state machine is on
state `i` and I see observation `o` and take action `a`, should the state
machine move to state `j`"?

#### State machines as world models

Given an observation, an action and an active state (e.g., the initial state),
a state machine can answer two questions:

- What will the next observation be if I take the action?
- Should the state machine transition to another state?

These two questions are implemented in `swmpo.state_machine.state_machine_model`.

### Policy Optimization

This implementation of SWMPO supports [Gymanasium](https://gymnasium.farama.org/index.html)'s API.

#### Augmenting MDPs

A Markov Decision Process can be augmented with its corresponding state
machine. The result is called the "product MDP". This is implemented in
`swmpo.gymnasium_wrapper.DeepSynthWrapper`.

#### Policy search

Any Reinforcement Learning algorithm can be used to find an approximately
optimal policy for the product MDP. For example, you can use the
implementations in [Stable-Baselines3](https://stable-baselines3.readthedocs.io/en/master/).
